#include <cuda_runtime_api.h>
#include <iostream>

int main()
{
    uchar4 *data;
    size_t stride;
    int width = 1024;
    int height = 768;
    cudaError_t err = cudaMallocPitch((void**)&buf, &stride, sizeof(uchar4)*width, height);

    if (err != cudaSuccess)
    {
        std::cerr << "failed to allocate! - " << cudaGetErrorName(err) << cudaGetErrorString(err);
    }

    cudaFree(buf);
}
